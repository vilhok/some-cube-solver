package soma;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import soma.model.Piece;
import soma.model.PieceInstance;
import soma.model.SomaTree;
import soma.model.SomaTree.SomaSolution;

public class Main {
	public static void main(String[] args) throws IOException{

		List<List<PieceInstance>> allPieces = new ArrayList<>();

		for (Piece p : Piece.values()){
			if (p == Piece.T){
				System.out.println("[skipping T due to only one valid location]");
				continue;
			}

			List<PieceInstance> pieces = p.getAllLocations();
			allPieces.add(pieces);
			System.out.println(String.format("Piece %s has %d valid locations", p, pieces.size()));
		}

		filterOverlappingWithTPiece(allPieces);

		System.out.println("pieces generated");

		SomaTree st = new SomaTree(Piece.T.getDefault());
		
		
		for (int i = 0; i < allPieces.size(); i++){
			List<PieceInstance> pieces = allPieces.get(i);
			Piece p = pieces.get(0).getPiece();
			if (p == Piece.T)
				continue;
			System.out.println("Adding piece:" + p);
			st.addPiece(pieces);
		}

		ArrayList<SomaSolution> solutions = st.traverse();
		HashMap<Character, Integer> middles = new HashMap<>();
		System.out.println(solutions.size());
		for (SomaSolution s : solutions){
			// System.out.println(s);
			Character c = s.middleChar();
			Integer count = middles.getOrDefault(c, 0);
			middles.put(c, count + 1);
			s.findMirror(solutions);
		}

		
		File root = new File("solutions");
		root.mkdir();
		for (SomaSolution s : solutions){
			File f = Paths.get(root.getAbsolutePath(), String.format("%03d", s.getID()) + ".txt").toFile();
			FileWriter fw = new FileWriter(f);
			// System.out.println(s.serializedData2());
			fw.write(s.serializedData2());

			//System.out.println(s.serializedData());
			System.out.println(s.toString());
			fw.close();
		}

	}

	/**
	 * As can be proven, the T piece has only one valid location relative to others:
	 * T piece must occupy two corners, as it can occupy 0 or 2 and the remaining
	 * pieces together can occupy maximum of 7 corners in any circumstances because
	 * of their shape.
	 * 
	 * Therefore each {@link PieceInstance} that overlaps with the T piece, can be
	 * filtered out. This reduces the overall running time as the final solution
	 * tree contains less tries.
	 * 
	 * 
	 * 
	 * @param allPieces the list of list of all PieceInstances
	 */
	public static void filterOverlappingWithTPiece(List<List<PieceInstance>> allPieces){
		PieceInstance tPiece = Piece.T.getDefault();

		for (List<PieceInstance> piece : allPieces){
			if (piece.get(0).getPiece() == Piece.T){
				// skip T piece
				continue;
			}
			ArrayList<PieceInstance> toRemove = new ArrayList<>();

			for (PieceInstance instance : piece){
				if (tPiece.overLaps(instance)){
					toRemove.add(instance);
				}
			}
			piece.removeAll(toRemove);
		}
	}

}
