package soma.model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import soma.model.Rotations.Matrix;

/**
 * Represents a single piece in a Soma cube.
 * 
 * Each piece consist of several cubes that are represented with points. The
 * whole soma cube is a 3x3x3 sized cube. These cubes are represented by
 * coordinate points and each piece occupies several of them.
 *
 */
public enum Piece {
	A('A', new Point[]{ new Point(0, 0, 0), new Point(1, 0, 0), new Point(0, 0, 1), new Point(0, 1, 1) }), //
	B('B', new Point[]{ new Point(0, 0, 0), new Point(1, 0, 0), new Point(0, 0, 1), new Point(1, 1, 0) }), //
	L('L', new Point[]{ new Point(0, 0, 0), new Point(1, 0, 0), new Point(0, 0, 1), new Point(0, 0, 2) }), //
	P('P', new Point[]{ new Point(0, 0, 0), new Point(1, 0, 0), new Point(0, 0, 1), new Point(0, 1, 0) }), //
	T('T', new Point[]{ new Point(0, 0, 0), new Point(1, 0, 0), new Point(2, 0, 0), new Point(1, 0, 1) }), //
	Z('Z', new Point[]{ new Point(0, 0, 0), new Point(1, 0, 0), new Point(1, 0, 1), new Point(2, 0, 1) }), //
	V('V', new Point[]{ new Point(0, 0, 0), new Point(1, 0, 0), new Point(0, 0, 1) });

	private final Point[] POINTS;

	final char LETTER;

	private Piece(char letter, Point[] points){
		this.POINTS = points;
		this.LETTER = letter;
	}

	/**
	 * Returns the piece instance in the default location. This is an arbitrary
	 * defined location in such way that one cube is placed in [0,0,0] coordinate,
	 * and others placed in such way that they fit inside the cube.
	 * 
	 * @return a {@link PieceInstance} that represents the default location of this
	 *         piece.
	 */
	public PieceInstance getDefault(){
		Point[] pts = new Point[POINTS.length];
		for (int i = 0; i < POINTS.length; i++){
			pts[i] = POINTS[i].copy();
		}
		return new PieceInstance(this, pts);
	}

	/**
	 * Returns a list of all possible ways this Piece can be placed inside a 3x3x3
	 * cube. The piece can be in any orientation and any location, as long as it
	 * fits inside the cube.
	 * 
	 * @return a list of distinct {@link PieceInstance} -objects that represent
	 *         distinct locations/orientations this piece can be placed in the
	 *         soma-cube.
	 */
	public List<PieceInstance> getAllLocations(){
		ArrayList<PieceInstance> instances = getAllRotations();
		ArrayList<PieceInstance> locations = new ArrayList<>();
		for (PieceInstance piece : instances){

			for (int i = 0; i < 3; i++){
				for (int j = 0; j < 3; j++){
					for (int k = 0; k < 3; k++){
						PieceInstance p = piece.copy();
						p.transX(i);
						p.transY(j);
						p.transZ(k);
						locations.add(p);
					}
				}
			}
		}
		return locations.stream().filter(p -> p.inside()).collect(Collectors.toList());
	}

	/**
	 * While a piece consists of several cube, it can be rotated in 3D-space. While
	 * solving the Soma-cube, the pieces and be in any orientation, as long as they
	 * link to each other. This method finds out all the orientations and provides
	 * an array of {@link PieceInstance} objects. All these instances represent a
	 * distinct orientation of the piece. A piece may be symmetrical within one or
	 * more axis. A unique orientation is defined solely by the collection of
	 * points, so rotating a symmerical piece by its axis does not count.
	 * 
	 * @return a list of distinct piece instances in all possible orientations.
	 * 
	 * @see Point
	 */
	public ArrayList<PieceInstance> getAllRotations(){
		ArrayList<PieceInstance> all = new ArrayList<>();

		for (int i = 0; i < 4; i++){
			for (int j = 0; j < 4; j++){
				for (int k = 0; k < 4; k++){
					PieceInstance p = getDefault();
					for (int a = 0; a < i; a++){
						p = p.copy(Matrix.ROT90_X);
					}
					for (int b = 0; b < j; b++){
						p = p.copy(Matrix.ROT90_Y);
					}
					for (int c = 0; c < k; c++){
						p = p.copy(Matrix.ROT90_Z);
					}
					p.fit();
					if (!all.contains(p))
						all.add(p);
				}
			}
		}
		return all;
	}
}